//
//  ViewController.swift
//  Act4
//
//  Created by Tecmilenio on 05/02/19.
//  Copyright © 2019 jfrn. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {

     var player = AVAudioPlayer()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        do {
            try player = AVAudioPlayer(contentsOf : URL(fileURLWithPath: Bundle.main.path(forResource:"abba_fernando", ofType: "mp3")!))
            
        } catch {
            
        }
        var playerTime : Timer!
        playerTime = Timer.scheduledTimer(timeInterval:0.1, target:self, selector:#selector(runTimedCode) , userInfo:nil , repeats:true)
    }
    
    
   /*---------------------------------SliderMusica--------------------------------------*/
    @IBOutlet weak var sliderMusic: UISlider!
    @IBAction func sliderMove(_ sender: UISlider) {
        sliderMusic.value = Float(player.currentTime)
    }
    
    @objc func runTimedCode(){
        var duration = Float(player.duration)
        var currentTime = Float(player.currentTime)
        sliderMusic.maximumValue = duration
    }
    
    /*---------------------------------CambioBtnPlayPause--------------------------------------*/
    var vali = true
    @IBOutlet weak var btnPlayPause: UIButton!
    @IBAction func play(_ sender: UIButton) {
        
        if vali == true{
            player.play()
            vali = false
            btnPlayPause.setTitle("Pause", for: .normal)
        }else{
            player.pause()
            vali = true
            btnPlayPause.setTitle("Play", for: .normal)
        }
    }
    
    /*---------------------------------SliderVolumen--------------------------------------*/
    
    @IBOutlet weak var sliderVolumen: UISlider!
    @IBAction func sliderVolume(_ sender: UISlider) {
        player.volume = sliderVolumen.value
    }
    
    /*---------------------------------BtnPrevious--------------------------------------*/
    
    @IBOutlet weak var previous: UIButton!
    @IBAction func btnPrevious(_ sender: UIButton) {
        if player.isPlaying{
            player.pause()
            player.currentTime = 0
            player.play()
        }else{
            player.play()
        }
    }
    /*----------------------------------------------------------------------------*/
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    


}

